# Crunchy

Node management tool with sane defaults

## Installation

### Development

Pre-requisites

| NAME | VERSON | NOTES         |
| ---- | ------ | ------------- |
| nvm  | latest | _recommended_ |

```bash
nvm install # recommended
nvm use # recommended
npm install
```
